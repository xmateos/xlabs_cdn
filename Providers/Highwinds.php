<?php

namespace XLabs\CDNBundle\Providers;

use Symfony\Component\HttpFoundation\RequestStack;
use XLabs\CDNBundle\CDNInterface;
use \Exception;

/*
 * HighWinds CDN
 */
class Highwinds implements CDNInterface
{
    const CDN_PROVIDER_CONFIG_KEY = 'highwinds';

    private $config = false;

    /*private $request;
    private $config;
    private $kernel_root_dir;

    public function __construct(RequestStack $request_stack, $config, $kernel_root_dir)
    {
        $this->request = $request_stack->getCurrentRequest();
        $this->config = $config;
        $this->kernel_root_dir = $kernel_root_dir;
    }*/

    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    public function getAsset($aOptions)
    {
        $default_options = array(
            'media_path' => false,
            'zone' => false,
            'ip_protection' => false,
            'expiration_ttl' => false,
            'force_download' => false
        );
        $aOptions = array_merge($default_options, $aOptions);

        $cdn_zone_name = $aOptions['zone'] ? $aOptions['zone'] : $this->config['default_zone'];
        if(!isset($this->config['zones'][$cdn_zone_name]))
        {
            throw new Exception('CDN zone "'.$cdn_zone_name.'" has not been defined for "'.self::CDN_PROVIDER_CONFIG_KEY.'" provider.');
        }

        $cdn_zone = $this->config['zones'][$cdn_zone_name];
        $baseURL = $cdn_zone['url'];
        $expire = $aOptions['expiration_ttl'] ? (time() + $aOptions['expiration_ttl']) : (time() + $cdn_zone['expiration_ttl']);

        //$expire = strtotime('-6 hours', time()) + 3600;
        $url = $baseURL.$aOptions['media_path'];
        $signing_url_params = array();
        if($aOptions['force_download'])
        {
            $signing_url_params['cd'] = 'attachment';
            if(is_string($aOptions['force_download']))
            {
                $signing_url_params['filename'] = $aOptions['force_download'];
            }
        }
        if(isset($cdn_zone['secretKey']))
        {
            $signing_url = $aOptions['media_path'];
            if($aOptions['ip_protection'])
            {
                $ip = trim($_SERVER['REMOTE_ADDR']);
                if($ip == '127.0.0.1')
                {
                    $externalContent = file_get_contents('http://checkip.dyndns.com/');
                    preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
                    $ip = trim($m[1]);
                }
                $signing_url_params['ip'] = ip2long($ip);
            }
            //$signing_url .= $aOptions['ip_protection'] ? 'ip='.ip2long(trim($_SERVER['REMOTE_ADDR'])).'&' : '';
            //$signing_url .= 's='.$this->session->getId().'&';
            $signing_url_params[$cdn_zone['ttl_param_name']] = $expire;
            if($cdn_zone['use_length_param'])
            {
                $signing_url_params[$cdn_zone['length_param_name']] = strlen($signing_url);
            }
            $signing_url_params['secret'] = $cdn_zone['secretKey'];
            $token = MD5($signing_url.'?'.http_build_query($signing_url_params));
            $url_params = $signing_url_params;
            unset($url_params['secret']);
            $url_params['token'] = $token;
            $url .= '?'.http_build_query($url_params);
        }
        return $url;
    }

    public function purgeAsset($arrFilePatterns)
    {
        $token = $this->getAuthenticationToken();

        $purgeURL = "https://striketracker.highwinds.com/api/accounts/".$this->config['account']."/purge";
        //Create send data
        $request_params = array(
            'list' => array()
        );
        foreach($arrFilePatterns as $filePattern)
        {
            $request_params['list'][] = array(
                'url' => $filePattern,
                'recursive' => true
            );
        }
        $data = json_encode($request_params);

        //Send the request to HW
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $purgeURL);
        curl_setopt($ch, CURLOPT_PORT , 443);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.$token,
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-length: '.strlen($data))
        );
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);
        return $httpCode && $httpCode['http_code'] == 200;
    }

    public function getAuthenticationToken()
    {
        // Authentication first
        $authURL = 'https://striketracker.highwinds.com/auth/token';
        $authData = 'username='.$this->config['api_username'].'&password='.$this->config['api_password'].'&grant_type=password';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $authURL);
        curl_setopt($ch, CURLOPT_PORT , 443);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $authData);

        $response = curl_exec($ch);
        curl_close($ch);
        $token = json_decode($response, true);
        return $token['access_token'];
    }
}