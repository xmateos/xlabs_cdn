<?php

namespace XLabs\CDNBundle\Providers;

use XLabs\CDNBundle\CDNInterface;
use \Exception;

/*
 * EuroRevenues CDN
 */
class EuroRevenue implements CDNInterface
{
    const CDN_PROVIDER_CONFIG_KEY = 'eurorevenue';

    private $config = false;

    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    public function getAsset($aOptions)
    {
        $default_options = array(
            'media_path' => false,
            'zone' => false,
            'expiration_ttl' => false
        );
        $aOptions = array_merge($default_options, $aOptions);

        $cdn_zone_name = $aOptions['zone'] ? $aOptions['zone'] : $this->config['default_zone'];
        if(!isset($this->config['zones'][$cdn_zone_name]))
        {
            throw new Exception('CDN zone "'.$cdn_zone_name.'" has not been defined for "'.self::CDN_PROVIDER_CONFIG_KEY.'" provider.');
        }

        $cdn_zone = $this->config['zones'][$cdn_zone_name];
        $baseURL = $cdn_zone['url'];

        $aOptions['media_path'] = str_replace('', '%20', $aOptions['media_path']);

        $secret = isset($cdn_zone['secretKey']) ? $cdn_zone['secretKey'] : '';
        $validFor = $aOptions['expiration_ttl'] ? $aOptions['expiration_ttl'] : $cdn_zone['expiration_ttl'];
        $stime = time() - $validFor;
        $etime = time() + $validFor;
        $token = "validfrom=$stime&validto=$etime&rate=20000k&burst=21000k";

        if(strpos($aOptions['media_path'],'?') === FALSE)
        {
            $aOptions['media_path'] = $aOptions['media_path']."?".$token;
        } else {
            $aOptions['media_path'] = $aOptions['media_path']."&".$token;
        }

        $hToken = urlencode(base64_encode(hash_hmac("sha1", $aOptions['media_path'], $secret, true)));

        if (strpos($aOptions['media_path'],'?') === FALSE) {
            return $baseURL.$aOptions['media_path']."?hash=".$hToken;
        } else {
            return $baseURL.$aOptions['media_path']."&hash=".$hToken;
        }
    }

    public function purgeAsset($arrFilePatterns)
    {
        return false;
    }
}