<?php

namespace XLabs\CDNBundle\Providers;

use XLabs\CDNBundle\CDNInterface;
use \Exception;

/*
 * Reflected CDN
 */
class Reflected implements CDNInterface
{
    const CDN_PROVIDER_CONFIG_KEY = 'reflected';

    private $config = false;

    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    public function getAsset($aOptions)
    {
        $default_options = array(
            'media_path' => false,
            'zone' => false,
            'expiration_ttl' => false
        );
        $aOptions = array_merge($default_options, $aOptions);

        $cdn_zone_name = $aOptions['zone'] ? $aOptions['zone'] : $this->config['default_zone'];
        if(!isset($this->config['zones'][$cdn_zone_name]))
        {
            throw new Exception('CDN zone "'.$cdn_zone_name.'" has not been defined for "'.self::CDN_PROVIDER_CONFIG_KEY.'" provider.');
        }

        $cdn_zone = $this->config['zones'][$cdn_zone_name];
        $baseURL = $cdn_zone['url'];

        if($cdn_zone_name == 'gallery_zip')
        {
            // special case, url with querystring looks like:
            // https://zipfiles-cdn.femjoy.com/126062/medium.zip?validfrom=1651554088&validto=1651770088&hash=QeGzUXEo5Fqyxp5UTTZ6BN4WooY%3D&download=1&filename=BERNIE_Ora_WinterDay_medium.zip
        }

        if($cdn_zone_name == 'download_mp4')
        {
            // special case, url with querystring looks like:
            // https://members-cdn.femjoy.com/25792/r-1280x720-mp4.mp4?validfrom=1651553419&validto=1651769419&hash=ERCPofO0tsK5NOEt3GTwMd5MO9A%3D&download=1&filename=Yogagirl_Vanity_1280x720_mp4.mp4
        }

        $secret = isset($cdn_zone['secretKey']) ? $cdn_zone['secretKey'] : '';
        $validFor = $aOptions['expiration_ttl'] ? $aOptions['expiration_ttl'] : $cdn_zone['expiration_ttl'];
        $stime = time() - $validFor;
        $etime = time() + $validFor;
        $token = "validfrom=$stime&validto=$etime";

        if(strpos($aOptions['media_path'],'?') === FALSE)
        {
            $aOptions['media_path'] = $aOptions['media_path']."?".$token;
        } else {
            $aOptions['media_path'] = $aOptions['media_path']."&".$token;
        }

        $hToken = urlencode(base64_encode(hash_hmac("sha1", $aOptions['media_path'], $secret, true)));

        if (strpos($aOptions['media_path'],'?') === FALSE) {
            return $baseURL.$aOptions['media_path']."?hash=".$hToken;
        } else {
            return $baseURL.$aOptions['media_path']."&hash=".$hToken;
        }
    }

    public function purgeAsset($arrFilePatterns)
    {
        return false;
    }
}