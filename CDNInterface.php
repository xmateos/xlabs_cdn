<?php

namespace XLabs\CDNBundle;

interface CDNInterface
{
    public function setConfig($config);
    public function getAsset($options);
    public function purgeAsset($aFilePatterns);
}