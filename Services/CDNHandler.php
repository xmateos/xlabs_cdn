<?php

namespace XLabs\CDNBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CDNHandler
{
    private $request;
    private $providers;
    private $config;
    private $kernel_root_dir;

    public function __construct(RequestStack $request_stack, iterable $providers, $config, $kernel_root_dir)
    {
        $this->request = $request_stack->getCurrentRequest();
        $this->providers = $providers;
        $this->config = $config;
        $this->kernel_root_dir = $kernel_root_dir;
    }

    private function getConfig($provider)
    {
        if(!empty($this->config) && $provider && array_key_exists('providers', $this->config) && array_key_exists($provider, $this->config['providers']))
        {
            return $this->config['providers'][$provider];
        }
        return false;
    }

    private function getDefaultProvider()
    {
        if(!empty($this->config) && array_key_exists('providers', $this->config))
        {
            //return reset($this->config['providers']);
            return array_key_first($this->config['providers']);
        }
        return false;
    }

    private function getProvider($provider)
    {
        foreach($this->providers as $_provider)
        {
            if($_provider::CDN_PROVIDER_CONFIG_KEY == $provider)
            {
                return $_provider;
            }
        }
        return false;
    }

    public function getCDNResource($aOptions)
    {
        $default_options = array(
            'media_path' => false,
            'zone' => false,
            'ip_protection' => false,
            'expiration_ttl' => false,
            'provider' => false,
            'path_stripping' => true,
            'path_renaming' => true
        );
        $aOptions = array_merge($default_options, $aOptions);

        $aOptions['provider'] = $aOptions['provider'] ? strtolower($aOptions['provider']) : $this->getDefaultProvider();
        if(!$aOptions['provider'])
        {
            return $aOptions['media_path'];
        }

        $config = $this->getConfig($aOptions['provider']);
        if(!$config)
        {
            return $aOptions['media_path'];
        }

        $provider = $this->getProvider($aOptions['provider']);
        if(!$provider)
        {
            throw new NotFoundHttpException('CDN provider not found');
        }

        // this commented code block has been resolved with nginx trick
        // if is local dev, check if the resource exists locally before checking the live cdn
        if($this->request)
        {
            $customLocalDevelopmentHeader = $this->request->headers->get('x-stiffia-local-dev');
            if($customLocalDevelopmentHeader)
            {
                $web_folder = realpath($this->kernel_root_dir.'/../web/');
                $toCheck = realpath($web_folder.$aOptions['media_path']);
                if(is_file($toCheck))
                {
                    return $aOptions['media_path'];
                }
            }
        }

        $aOptions['zone'] = $aOptions['zone'] ? $aOptions['zone'] : $config['default_zone'];

        // Path strippings
        if($aOptions['path_stripping'] && isset($config['path_stripping']))
        {
            foreach($config['path_stripping'] as $path_renaming)
            {
                $aOptions['media_path'] = preg_replace('!'.$path_renaming['source'].'!', $path_renaming['dest'], $aOptions['media_path']); // regexp
            }
        }

        // Path renamings
        if($aOptions['path_renaming'] && isset($config['path_renaming']))
        {
            foreach($config['path_renaming'] as $path_renaming)
            {
                if(!empty($path_renaming['zones']))
                {
                    if(in_array($aOptions['zone'], $path_renaming['zones']))
                    {
                        $aOptions['media_path'] = preg_replace('!'.$path_renaming['source'].'!', $path_renaming['dest'], $aOptions['media_path']); // regexp
                    }
                } else {
                    $aOptions['media_path'] = preg_replace('!'.$path_renaming['source'].'!', $path_renaming['dest'], $aOptions['media_path']); // regexp
                    //$aOptions['media_path'] = str_replace($path_renaming['source'], $path_renaming['dest'], $aOptions['media_path']);
                }
            }
        }

        return $provider->setConfig($config)->getAsset($aOptions);
    }

    public function purge($aOptions)
    {
        $default_options = array(
            'aFilePatterns' => array(),
            'provider' => false
        );
        $aOptions = array_merge($default_options, $aOptions);

        $aOptions['provider'] = $aOptions['provider'] ? strtolower($aOptions['provider']) : $this->getDefaultProvider();
        if(!$aOptions['provider'])
        {
            return false;
        }

        $config = $this->getConfig($aOptions['provider']);
        if(!$config)
        {
            return false;
        }

        $provider = $this->getProvider($aOptions['provider']);
        if(!$provider)
        {
            return false;
        }

        return $provider->setConfig($config)->purgeAsset($aOptions['aFilePatterns']);
    }
}