<?php

namespace XLabs\CDNBundle\Extension;

use XLabs\CDNBundle\Services\CDNHandler;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CDNExtension extends AbstractExtension
{
    private $xlabs_cdn;
    
    public function __construct(CDNHandler $xlabs_cdn)
    {
        $this->xlabs_cdn = $xlabs_cdn;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getCDNResource', array($this, 'getCDNResource')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function getCDNResource($aOptions)
    {
        /* Local dev assets load */
        /*if(strlen(strstr($aOptions['media_path'], 'assets/images')) > 0)
        {
            return $aOptions['media_path'];
        }*/
        /* END - Local dev assets load */
        return $this->xlabs_cdn->getCDNResource($aOptions);
    }
}