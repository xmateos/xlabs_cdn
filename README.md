CDN wrapper engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/cdnbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\CDNBundle\XLabsCDNBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_cdn:
    providers:
        highwinds:
            account: ############
            api_username: ############
            api_password: ############
            zones:
                sample_public: {url: 'https://cdn.static.yourdomain.ext'}
                sample_private: {url: 'https://cdn.yourdomain.ext', secretKey: '############', expiration_ttl: 7200}
                sample_download: {url: 'https://cdn-file.yourdomain.ext', secretKey: '############', expiration_ttl: 7200}
                ...
            default_zone: 'sample_public'
            path_renaming: # optional
                - { source: '^/my/path/', dest: '/cdn/rewrite/rule/path/'}
                - { source: '...', dest: '...'}
        provider2:
            param1: #######
            param2: #######
            ...
            paramN: #######
        ...
```

### Usage ###
Instantiate the service from a Controller by:
```php
$this->get('xlabs_cdn')->...
```
### Usage (TWIG) ###
Instantiate the service from a twig template by:
```php
{{ getCDNResource({
    'media_path' => '/path/to/my/asset.ext',
    'zone' => false, // default_zone will be used if unset
    'expiration_ttl' => 7200 // default ttl (7200) will be used if unset
}) }}
```
### Path renamings ###
Make sure to create the as alias in the cdn vhost, so that "/cdn/rewrite/rule/path/" is an alias of "/my/path/".
### Path strippings ###
For instance, when using S3 storage, assets in db might contain path "media/project_name".
If the CDN zone was already pointing to that "media/project_name" folder, it would need to be stripped from the requested asset, before signing url.