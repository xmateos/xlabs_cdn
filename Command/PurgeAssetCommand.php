<?php

namespace XLabs\CDNBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PurgeAssetCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('purge:asset')
            ->addArgument('asset_url',InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset = $input->getArgument('asset_url');

        $xlabs_cdn = $this->getContainer()->get('xlabs_cdn');
        $purge_success = $xlabs_cdn->purge(array(
            $asset
        ));

        dump($purge_success);
    }
}